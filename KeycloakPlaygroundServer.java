package com.dapeng.cloud;


import org.keycloak.common.Version;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.keycloak.testsuite.KeycloakServer;

import java.text.SimpleDateFormat;
import java.util.Date;

public class KeycloakPlaygroundServer {
  public static void main(String[] args) throws Throwable {

    System.out.println("启动 KeycloakPlaygroundServer");
    Version.BUILD_TIME = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
    System.setProperty("resources", "/home/songkaizong/IdeaProjects/keycloak-dapeng-extension/dapeng-theme/src/main/resources");
    System.setProperty("keycloak.theme.dir", "/home/songkaizong/IdeaProjects/keycloak-dapeng-extension/dapeng-theme/src/main/resources/themes");
    KeycloakServer keycloakServer = KeycloakServer.bootstrapKeycloakServer(args);
    RealmRepresentation myRealm = new RealmRepresentation();
    myRealm.setRealm("myRealm");
    ClientRepresentation client=new ClientRepresentation();
    client.setId("app1");
    UserRepresentation user=new UserRepresentation();
    user.setUsername("skz");
//    myRealm.setUsers();
//    myRealm.setClients();
//    keycloakServer.importRealm();
  }

}

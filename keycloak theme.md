# 1.开发方式
#### 主题类型
- Account - Account management
- Admin - Admin console
- Email - Emails
- Login - Login forms
- Welcome - Welcome page
#### 创建主题
- HTML templates (Freemarker Templates)
- Images
- Message bundles
- Stylesheets
- Scripts
- Theme properties
#### 主题开发
- 严格按照文件目录，文件名称，文件格式，可选的继承父主题。
#### 调整主题
- 继承父主题，选择性的覆盖或增加表单控件，更改样式，更改文字内容等。
#### 完全覆盖开发
- 基于freemarker模板引擎及vue等当下流行纯前端技术。
- 找到原页面中出现的接口。
- 找到并理解原页面中出现的表达式及含义。

## 2.难点
**难点在于找到并理解原页面中出现的表达式及含义。**
#### 挖掘模板引擎中realm.password表达式含义：

在login.ftl中有如下代码:
```html
  <#if realm.password>
          <form id="kc-form-login" onsubmit="login.disabled = true; return true;" action="${url.loginAction}"
                method="post">
                .............
 <#if>
```
可以看到有一个realm.password表达式，如果realm.password为ture，则展示登录表单。
经过查找源码其计算依赖的是realm model中的getRequiredCredentials()，经过研究其对应的应当是官方文档中描述的credentials属性，解释如下：

> Specify the credentials of the application. This is an object notation where the key is the credential type and the value is the value of the credential type. Currently password and jwt is supported. This is REQUIRED only for clients with 'Confidential' access type.

其实际值与client中的Access Type属性相关：
>Access Type  
This defines the type of the OIDC client.
> - confidential  
Confidential access type is for server-side clients that need to perform a browser login and require a client secret when they turn an access code into an access token, (see Access Token Request in the OAuth 2.0 spec for more details). This type should be used for server-side applications.  
>- public  
Public access type is for client-side clients that need to perform a browser login. With a client-side application there is no way to keep a secret safe. Instead it is very important to restrict access by configuring correct redirect URIs for the client.  
>- bearer-only  
Bearer-only access type means that the application only allows bearer token requests. If this is turned on, this application cannot participate in browser logins.  

结果就是：如果client的Access Type为public ，则realm.password 为true。

当然可以无视这个变量，去写自己的页面，但是：

- 如果不去理，就会破坏keycloak的内部逻辑，比如后端控制台修改操作的失效。
- 如果完全按照keycloak开发，则需要理清楚每一项配置的含义。


docker run --name keycloak -p8888:8080   -d\
 -v /home/songkaizong/dockerV/keycloak_mytheme:/opt/jboss/keycloak/themes/mytheme  \
-e KEYCLOAK_USER=admin   \
-e KEYCLOAK_PASSWORD=admin  \
-e DB_VENDOR=mysql  \
-e DB_ADDR=101.200.216.102   \
-e DB_PORT=3306    \
-e DB_DATABASE=keycloak  \
-e DB_USER=root  \
-e DB_PASSWORD=956836  \
jboss/keycloak 
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>登录</title>
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <style type="text/css">
    [v-cloak] {
      display: none !important;
    }

    .el-checkbox__label {
      font-size: 13px;
    }

    .el-tabs__item {
      font-size: 15px;
    }

    .el-radio__label {
      font-size: 16px;
      vertical-align: middle;
    }
  </style>
</head>
<body>
<div id="app" style="width: 440px" v-cloak="">
  <div style="width: 316px;margin: auto">
    <!--多线索选择用户-->
    <el-form label-width="80px" method="post" action="/account-login" v-if="userList.length>0">
      <p style="font-size: 18px;color: #75737E;margin: 25px auto 0 auto">您的手机号绑定多个身份，<br>请选择您要登录的账号：</p>
      <el-radio-group v-model="selectUserId">
        <el-radio :label="item.userId" v-for="item in userList" style="margin-top: 25px;">
          {{item.nickName}}(手机号:{{item.mobile}})
        </el-radio>
      </el-radio-group>
      <span v-show="false">
      <el-input name="source" value="NORMALLOGIN"></el-input>
      <el-input name="type" value="USERID"></el-input>
      <el-input name="account" v-model="selectUserId"></el-input>
      <el-input name="password" v-model="selectPassword"></el-input>
      </span>
      <el-button type="primary" :disabled="selectUserId==''" native-type="submit" style="width: 100%;margin-top: 30px">
        就决定这个了
      </el-button>
      <div style="color: #00B93B;font-size: 13px;text-align: center;margin-top: 20px;cursor:pointer">
        找不到我的账号？重新以用户名登录
      </div>
    </el-form>
    <!--登录表单-->
    <el-form ref="login" :model="login" label-width="0px" id="loginForm"
             method="post" action="${url.loginAction}" v-else>
      <el-tabs v-model="loginType" style="margin: 35px auto 15px auto;width: 302px">
        <el-tab-pane label="账号登录" name="account"></el-tab-pane>
        <el-tab-pane label="验证码登录" name="mobile"></el-tab-pane>
        <el-tab-pane label="微信/QQ登录" name="qq"></el-tab-pane>
      </el-tabs>
      <el-input name="source" v-show="false" v-model="login.source"></el-input>
      <el-input name="type" v-show="false" v-model="login.type"></el-input>
      <template v-if="loginType=='account'">
        <el-form-item>
          <el-input placeholder="请输入您的大鹏号/用户名/手机号" v-model="login.account" name="username">
            <i slot="prefix" class="el-icon-user"></i>
          </el-input>
        </el-form-item>
        <el-form-item>
          <el-input placeholder="请输入密码" v-model="login.password" name="password" show-password>
            <i slot="prefix" class="el-icon-lock"></i>
          </el-input>
        </el-form-item>
        <div style="margin-top: 10px">
          <el-checkbox v-model="login.rememberMe">下次自动登录</el-checkbox>
          <span style="color: #00B93B;float: right;font-size: 13px;cursor:pointer ">找回密码</span>
        </div>
        <el-input name="rememberMe" v-model="login.rememberMe" v-show="false"></el-input>
      </template>
      <template v-if="loginType=='mobile'">
        <el-form-item label="">
          <el-input placeholder="请输入您绑定过的手机号" v-model="login.account" name="account">
            <i slot="prefix" class="el-icon-mobile-phone"></i>
          </el-input>
        </el-form-item>
        <el-form-item label="">
          <el-row>
            <el-col span="14">
              <el-input placeholder="请输入验证码" v-model="login.password" name="password">
                <i slot="prefix" class="el-icon-lock"></i>
              </el-input>
            </el-col>
            <el-col span="8">
              <el-button type="primary" @click="sendCode" :disabled="isSending">
                {{codeText}}
              </el-button>
            </el-col>
          </el-row>
        </el-form-item>
      </template>
      <template v-if="loginType=='qq'">
        <el-form-item>
          <el-input placeholder="请输入QQ或微信号" v-model="login.account" name="account">
            <i slot="prefix" class="el-icon-user"></i>
          </el-input>
        </el-form-item>
        <el-form-item>
          <el-input placeholder="请输入登录密码" v-model="login.password" name="password" show-password>
            <i slot="prefix" class="el-icon-lock"></i>
          </el-input>
        </el-form-item>
        <div style="color: #00B93B;font-size: 13px;cursor:pointer;margin-top: 10px;text-align: right">找回密码</div>
      </template>
      <div style="color: #FF9466;margin-top: 20px;font-size: 12px;">{{errorMsg}}</div>
      <el-form-item>
        <el-button type="primary" style="width: 100%;margin-top: 50px" @click="submitLogin">快速登录</el-button>
      </el-form-item>
    </el-form>

    <div v-if="userList.length==0">
      <div style="color: #00B93B;font-size: 14px;cursor:pointer ;text-align: center">
        第一次加入大鹏?点这里注册
      </div>
      <div style="font-size: 12px;text-align: center;width: 240px;margin: 30px auto 0 auto">
        <span>点击登录或完成账号注册表示您已阅读并同意大鹏</span>
        <span style="color: #00B93B;cursor: pointer" @click="toProtocol">《用户协议》</span>
        <span>及</span>
        <span style="color: #00B93B ;cursor: pointer" @click="toPolicy">《隐私政策》</span>
      </div>
    </div>
  </div>

</div>
<script src="https://cdn.bootcss.com/vue/2.6.10/vue.min.js"></script>
<script src="https://cdn.bootcss.com/element-ui/2.12.0/index.js"></script>
<script src="https://cdn.bootcss.com/axios/0.18.1/axios.min.js"></script>
<script>
  let app = new Vue({
    el: "#app",
    data: {
      errorMsg: ' ',
      userList: [],
      selectUserId: '',
      //多线索登录密码
      selectPassword: '',
      codeText: '获取验证码',
      isSending: false,
      lazyTime: 60,
      login: {
        account: '',
        password: '',
        source: 'NORMALLOGIN',
        type: '',
        rememberMe: false
      },
      loginType: 'account',
    },
    methods: {
      submitLogin() {
        if (this.login.account === "") {
          this.errorMsg = "请输入账号";
          return;
        }
        if (this.login.password === "") {
          this.errorMsg = "请输入密码";
          return;
        }
        localStorage.setItem("loginType", this.loginType);
        localStorage.setItem("account", this.login.account);
        window.document.getElementById("loginForm").submit();
      },
      toProtocol() {
        window.open("https://test.dapengjiaoyu.com/protocol/lastest");
      },
      toPolicy() {
        window.open("https://test.dapengjiaoyu.com/policy/lastest");
      },
      checkPhone(phone) {
        return (/^1[3456789]\d{9}$/.test(phone));
      },
      sendCode() {
        if (!this.checkPhone(this.login.account)) {
          this.errorMsg = "请填写正确的手机号";
          return;
        }
        let _this = this;
        _this.isSending = true;
        axios.post("/send-code?type=sms&mobile=" + this.login.account).then(function (response) {
          _this.lazyCode();
        }).catch(function (error) {
          _this.isSending = false;
          alert("发送验证码请求失败");
        });
      },
      lazyCode() {
        let _this = this;
        if (this.lazyTime === 0) {
          this.codeText = "获取验证码";
          this.isSending = false;
          this.lazyTime = 60;
        } else {
          this.lazyTime--;
          this.codeText = "重新获取(" + this.lazyTime + "s)";
          setTimeout(function () {
            _this.lazyCode();
          }, 1000)
        }
      }
    },
    watch: {
      loginType: function (value) {
        this.errorMsg = "";
        if (value === "account") {
          this.login.source = "NORMALLOGIN";
          this.login.type = "";
        } else if (value === "mobile") {
          this.login.source = "NORMALLOGIN";
          this.login.type = "SMS";
        } else if (value === "qq") {
          this.login.source = "ABNORMALLOGIN";
          this.login.type = "";
        }
      }
    },
    created: function () {
      <#--let loginAction = '${url.loginAction}'-->
      <#--console.log(loginAction);-->
      <#--var realm = '${realm}';-->
      <#--console.log(realm);-->
      <#--console.log(111);-->
      <#--var password = '${realm.password}';-->
      // console.log(password);
    }
  })
</script>
</body>
</html>
